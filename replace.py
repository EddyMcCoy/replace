#!/usr/bin/python************************************************************* #
#                                                                              #
#                                                         :::      ::::::::    #
#    replace.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/03 13:56:46 by gfanton           #+#    #+#              #
#    Updated: 2014/04/16 14:14:50 by Eddy             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys, os

if (len(sys.argv) < 4):
    print "fail: [path] [str_to_replace] [str] [[recurse]] [[all] [ext]] [[ext]]"
    sys.exit()

OPTN = {"r" : False, "a" : False}
EXT = ['c', 'h', 'php', 'html']
if (len(sys.argv) > 4):
    i = 4
    for arg in sys.argv[i:]:
        i += 1
        if (arg == "recurse" or arg == "r" or arg == "R"):
            OPTN['r'] = True
        elif (arg == "all" or arg == "a"):
            OPTN['a'] = True
        elif (arg == "e" or arg == "ext" or arg == "extenstion"):
            EXT = []
            for arg in sys.argv[i:]:
                EXT.append(arg)
            break
        else:
            print "unkmow optn: [{0}]".format(arg)
            sys.exit()

PATH = sys.argv[1]
STR1 = sys.argv[2]
STR2 = sys.argv[3]

def replace(line, i):
    if (STR1 in line):
        color1 = "\033[1;35m{0}\033[0m".format(STR1)
        print "[\033[1;35m{0}\033[0m]: \t\033[1;31mreplacing: \033[0m[\033[1;31m{1}\033[0m]".format(str(i), line.strip().replace(STR1, "\033[1;35m{0}\033[1;31m".format(STR1)))
        line = line.replace(STR1, STR2)
        print "\033[1;32m\t\t>> \033[0m[\033[1;32m{0}\033[0m]".format(line.strip().replace(STR2, "\033[1;35m{0}\033[1;32m".format(STR2)))
        print" -- "
    return line

def open_file(file):
    if (OPTN['a'] or file.split(".")[-1] in EXT):
        new_file = ""
        print "\033[1;34m-> reading: " + os.path.relpath(file) + "\033[0m"
        with open(file, "rw") as f:
            src = f.readlines()
            i = 0
            for line in src:
                i += 1
                new_file += replace(line, i)
        with open(file, "w") as f:
            f.write(new_file)

def open_and_replace(path):
    path = os.path.abspath(path)
    if (os.path.isfile(path)):
        open_file(path)
    elif (os.path.isdir(path)):
        if (path[-1] != "/"):
            path += "/"
        files = os.listdir(path)
        for fil in files:
            if (os.path.basename(fil)[0] != '.'):
                if (os.path.isdir(path + fil) and OPTN['r']):
                    open_and_replace(path + fil)
                else:
                    open_file(path + fil)
    else:
        print "fail: " + path + "is not valid"
        sys.exit()

open_and_replace(PATH)
