#!/usr/bin/python
#***************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    find.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gfanton <gfanton@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/03 13:56:46 by gfanton           #+#    #+#              #
#    Updated: 2014/04/16 14:14:50 by Eddy             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import sys, os

if (len(sys.argv) < 3):
    print "fail: [path] [str] [[verbose]] [[size]] [[recurse]] [[all]] [ext]"
    sys.exit()

OPTN = {"r" : False, "a" : False, "s" : 5, "v": False}
EXT = ['c', 'h', 'php', 'html']
if (len(sys.argv) > 3):
    i = 3
    for arg in sys.argv[i:]:
        i += 1
        if (arg == "recurse" or arg == "r" or arg == "R"):
            OPTN['r'] = True
        elif (arg == "all" or arg == "a"):
            OPTN['a'] = True
        elif (arg == "v" or arg == "verbose"):
            OPTN['v'] = True
        elif (arg == "size" or arg == "s"):
            OPTN['s'] = int(sys.argv[i])
            i += 1
            break
        elif (arg == "e" or arg == "ext" or arg == "extenstion"):
            EXT = []
            for arg in sys.argv[i:]:
                EXT.append(arg)
            break

PATH = sys.argv[1]
STR = sys.argv[2]
SIZE = 5


def printRange(src, line, area):
    ret = ""
    if (line >= 0 and line < len(src)):
        i = line - (area / 2) if line - (area / 2) >= 0 else 0
        for n in range(area):
            if (i >= len(src)):
                return i;
            ret += "\033[1;{0}m{1}\033[0m: \033[1;31m{2}\033[0m\t".format(32 if STR in src[i] else 35, i, src[i].strip().replace(STR, "\033[1;32m{0}\033[1;31m".format(STR))) + '\n'
            i += 1
    ret += "\n"
    return ret;

def search(src):
    ret = ""
    for i in range(len(src)):
        if (STR in src[i]):
            ret += str(printRange(src, i, OPTN['s']))

    return ret

def open_file(fil):
    if (OPTN['a'] or fil.split(".")[-1] in EXT):
        with open(fil, "r") as f:
            src = f.readlines()
            ret = search(src)
            if ret != "":
                print "\033[1;34m-> reading: " + os.path.relpath(fil) + "\033[0m"
                print ret

def open_and_replace(path):
    path = os.path.abspath(path)
    if (os.path.isfile(path)):
        open_file(path)
    elif (os.path.isdir(path)):
        if (path[-1] != "/"):
            path += "/"
        files = os.listdir(path)
        for fil in files:
            try:
                if (os.path.basename(fil)[0] != '.'):
                    if (os.path.isdir(path + fil) and OPTN['r']):
                        open_and_replace(path + fil)
                    else:
                        open_file(path + fil)
            except OSError as e:
                if OPTN['v']:
                    print "\033[1;32m[verbose]\033[0m\033[0;31m" + str(e) + "\033[0m"
    else:
        print "fail: " + path + "is not valid"
        sys.exit()

open_and_replace(PATH)
