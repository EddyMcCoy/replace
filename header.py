# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    header.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: joriscaca <joriscaca@student.42.fr>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/17 19:25:04 by joriscaca         #+#    #+#              #
#    Updated: 2014/06/17 15:15:56 by gfanton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/usr/bin/python

import sys, os, re, time

l1 = "**************************************************************************\n"
l2 = "                                                                          \n"
l3 = "                                                       :::      ::::::::  \n"
l4 = "  {0}{1}:+:      :+:    :+:  \n"
l5 = "                                                   +:+ +:+         +:+    \n"
l6 = "  By: {0} <{1}@student.42.fr>{2}+#+  +:+       +#+       \n"
l7 = "                                               +#+#+#+#+#+   +#+          \n"
l8 = "  Created: {0} by {1}{2}#+#    #+#            \n"
l9 = "  Updated: {0} by {1}{2}###   ########.fr      \n"
l10 = "                                                                          \n"
l11 = "**************************************************************************"

if (len(sys.argv) < 3):
    print "fail: [path] [user] [[all?]]"
    sys.exit()

if (len(sys.argv) == 4):
    if (sys.argv[3] == "all"):
        OPTN = "all"
    else:
        print "unkmow optn: [{0}]".format(sys.argv[3])
        sys.exit()
else:
    OPTN = "NONE"

PATH = sys.argv[1]
STR1 = sys.argv[2]
EXT = [["c", "^(.+\.c)$", "/* ", " */"],
       ["h", "^(.+\.h)$", "/* ", " */"],
       ["Makefile", "^Makefile$", "#**", "**#"],
       ["default", ".+", "# *", "* #"]]

def creat_header(name, user):
    header = ""
    header += l1 + l2 + l3
    l44 = l4.format(name, (75 - (len(l4) - 6) - len(name)) * " ")
    header += l44 + l5
    l66 = l6.format(user, user, (75 - (len(l6) - 9) - len(user) * 2) * " ")
    header += l66 + l7
    t = time.strftime("%Y/%m/%d %H:%M:%S", time.localtime())
    l88 = l8.format(t, user, (75 - (len(l8) - 9) - len(user) - len(t)) * " ")
    l99 = l9.format(t, user, (75 - (len(l9) - 9) - len(user) - len(t)) * " ")
    header += l88 + l99 + l10 + l11
    return (header.split('\n'))

def format_header(name, header):
    for e in EXT:
        if (re.match(e[1], name)):
            if (e[0] == "default" and OPTN == "NONE"):
                return (None)
            for i in range(len(header)):
                header[i] = e[2] + header[i] + e[3]
            return header
    return None

def erase_header(fil):
    for i in range(10):
        if (re.match("^(((# )|(/\*)).{76}(( #)|(\*/)))$", fil[i]) == None):
            if (i >= 1):
                print "  --> /!\\ WRONG HEADER DETECTED /!\\"
                ret = ""
                while (ret != 'y' and ret != 'n'):
                    ret = raw_input("continue ? (y/n): ")
                    if (ret == 'n'):
                        sys.exit()
            return fil
    print "  --> erasing old header"
    return fil[11:]

def open_file(fil):
    print "+---- reading: " + fil + " ----+"
    with open(fil, "rw") as f:
        src = f.readlines()
        if (src):
            src = erase_header(src)
            header = creat_header(os.path.basename(fil), STR1)
            header = format_header(os.path.basename(fil), header)
            if (header):
                for i in range(len(header)):
                    header[i] += '\n'
                src = header + src
                with open(fil, "w") as f:
                    f.write("".join(src))
                print "  --> header added"
            else:
            	print "  --> not a valid file (use 'all' optn)"
    print "\n"

def open_and_replace(path):
    path = os.path.abspath(path)
    if (os.path.isfile(path)):
        open_file(path)
    elif (os.path.isdir(path)):
        if (path[-1] != "/"):
            path += "/"
        files = os.listdir(path)
        for fil in files:
            if (os.path.basename(fil)[0] != '.'):
                if (os.path.isdir(path + fil)):
                    open_and_replace(path + fil)
                else:
                    open_file(path + fil)
    else:
        print "fail: " + path + "is not valid"
        sys.exit()

open_and_replace(PATH)
